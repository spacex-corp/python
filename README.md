# flywheel/python-slim and flywheel/python-alpine

Python base images with a common basic toolset for Flywheel projects.

The basic toolset includes:

- [`bash`](https://github.com/bminor/bash)
- [`curl`](https://github.com/curl/curl)
- [`git`](https://github.com/git/git)
- [`gosu`](https://github.com/tianon/gosu)
- [`jq`](https://github.com/stedolan/jq)
- [`nano`](https://github.com/madnight/nano)
- [`poetry`](https://github.com/python-poetry/poetry)
- [`tini`](https://github.com/krallin/tini)
- [`tree`](https://github.com/nodakai/tree-command)

Two image variants are available:

- `flywheel/python-slim` based on `python:3.8-slim-buster` - the recommended
base image hitting a sweet spot between image size and build complexity / time
due to `glibc` and the support of pre-built wheels.
- `flywheel/python-alpine` based on `python:3.8-alpine` - for projects where a
small image size is important or otherwise the lack of pre-built wheels and the
use of `musl` is not a [concern](https://pythonspeed.com/articles/alpine-docker-python/).

## Usage

Create a `Dockerfile` within your python project:

```plaintext
FROM flywheel/python-slim:master
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]

# install project dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry install --no-dev --no-root

# install project
COPY my-project ./
RUN poetry install --no-dev

# use a valid init process and step-down from root
CMD ["tini", "--", "gosu", "flywheel", "my-cmd"]
```

Create a `.dockerignore` file that limits the build context:

```plaintext
**
!my-project
!poetry.lock
!pyproject.toml
```

## Development

Edit [`Dockerfile.slim`](https://gitlab.com/flywheel-io/tools/docker/python/-/blob/master/Dockerfile.slim)
and [`Dockerfile.alpine`](https://gitlab.com/flywheel-io/tools/docker/python/-/blob/master/Dockerfile.alpine)
while keeping the common toolset in sync between the two.
Finally make sure you run the [`pre-commit`](https://pre-commit.com/) hooks:

```bash
pre-commit run --all-files
```

## Publishing

Images are published on dockerhub after every successful CI build:

- [flywheel/python-slim](https://hub.docker.com/repository/docker/flywheel/python-slim/tags?page=1&ordering=last_updated)
- [flywheel/python-alpine](https://hub.docker.com/repository/docker/flywheel/python-alpine/tags?page=1&ordering=last_updated)

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)

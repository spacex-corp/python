#!/usr/bin/env bash
# Update apt, apk and pip package version pins in a Dockerfile.
# Usage: ./update_dockerfile.sh <Dockerfile>
test -z "$TRACE" || set -x
set -euo pipefail


main() {
    DOCKERFILE="$1"
    HASH=$(md5sum "$DOCKERFILE")
    PKGS="bash curl git jq nano tini tree"
    if command -v apk >/dev/null 2>&1; then
        PKG_CMD=get_apk_version
        PKGS="$PKGS su-exec"
    else
        PKG_CMD=get_apt_version
        PKGS="$PKGS gosu iputils-ping"
        apt-get -qy update
    fi
    for PKG in $PKGS; do
        sed -Ei "s/$PKG=\S+/$PKG=$($PKG_CMD "$PKG")/" "$DOCKERFILE"
    done
    PIP_PKGS="$(grep "==" "$DOCKERFILE" | sed -E "s/\s+(\S+)==.*/\1/")"
    for PKG in $PIP_PKGS; do
        sed -Ei "s/$PKG==\S+/$PKG==$(get_pip_version "$PKG")/" "$DOCKERFILE"
    done
    sed -Ei "s/(POETRY_VERSION=).*/\1$(get_pip_version poetry)/" "$DOCKERFILE"
    if [ "$(md5sum "$DOCKERFILE")" = "$HASH" ]; then
        echo "$DOCKERFILE already up to date"
    else
        echo "$DOCKERFILE updated"
        exit 1
    fi
}


# get latest apk package version by name
get_apk_version() {
    echo "resolving apk $PKG ..." >&2
    apk --no-cache search --exact "$1" | grep -v fetch | grep -Eo "[0-9].*"
}


# get latest apt package version by name
get_apt_version() {
    echo "resolving apt $PKG ..." >&2
    apt-cache policy "$1" | grep Candidate | grep -Eo "[0-9].*"
}


# get latest pip package version by name
get_pip_version() {
    echo "resolving pip $PKG ..." >&2
    pip --no-cache-dir search "$1" | grep "^$1 (" | sed -E "s/.*\((.*)\).*/\1/"
}


main "$@"
